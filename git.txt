//先在gitlab 伺服器創建一個新的repository
參考網址:https://sakananote2.blogspot.com/2020/08/vscode-gitlab.html

//SSH key 金鑰(若是沒有的話，要記得建立)
ssh-keygen
test delete branch


//設定git 資訊 配置你的 username 與 email　(與 GitHub 帳號、Email 一致)
git config --global user.name "user name" 
git config --global user.email "user email" 
ex:
git config --global user.name "tony123b11"
ex:
git config --global user.email "tony123b11@gmail.com"

//將repository複製下賴到本機
git clone repositoryURL
ex:git clone git@gitlab.com:tony123b11/ssh.git

//如果沒有存在的SSH key
ssh-keygen
Enter file in which to save the key (C:\Users\GA4842/.ssh/id_rsa): 可以設定名稱

//初始化 a new repository
git init

//連接遠端repository儲存庫(SSH網址)
git remote add repositoryName repositoryURL
ex:
git remote add origin git@gitlab.com:tony123b11/ssh.git

//刪除 remote
git remote remove repositoryName
ex:
git remote remove origin

//查詢git remote是否綁定成功
git remote -v

//將所有檔案加入暫存(stage)
git add .

//提交版本，並寫下Commit
git commit =m ""
ex:git commit -m "git push code!!"

//把檔案push到要去的分支或者主支
git push repositoryName refspec
ex:
git push -u origin main


//參考網址
SSH密要建立:https://medium.com/devops-with-valentine/2021-how-to-your-ssh-key-for-gitlab-on-windows-10-587579192be0
git操作指令參考:https://rommelhong.medium.com/%E4%B8%83%E5%88%86%E9%90%98%E5%AD%B8%E6%9C%83gitlab-ecdcbcb42b9c
